EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L cheesebox-rescue:R_POT-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue RV1
U 1 1 5D6D91FB
P 3950 6300
F 0 "RV1" H 3880 6254 50  0000 R CNN
F 1 "47k" H 3880 6345 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3950 6300 50  0001 C CNN
F 3 "~" H 3950 6300 50  0001 C CNN
	1    3950 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 6150 3950 6100
Wire Wire Line
	3950 6450 3950 6500
Text GLabel 4400 800  1    50   Input ~ 0
fpwm1
Text Notes 3750 5850 0    50   ~ 0
Value set
$Comp
L 74xx:74HC4051 U5
U 1 1 5DBB2BBF
P 7650 2950
F 0 "U5" H 7700 3500 50  0000 L CNN
F 1 "74HCT4051" H 7700 3400 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7650 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 7650 2550 50  0001 C CNN
	1    7650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3550 7650 3600
Wire Wire Line
	7650 3600 7300 3600
Wire Wire Line
	7300 3600 7300 3250
Wire Wire Line
	7300 3050 7350 3050
Wire Wire Line
	7350 3250 7300 3250
Connection ~ 7300 3250
Wire Wire Line
	7300 3250 7300 3050
Connection ~ 7650 3600
Wire Wire Line
	7350 2650 7300 2650
Wire Wire Line
	5500 3450 5500 3600
Wire Wire Line
	5600 3600 5600 3450
Wire Wire Line
	7750 3600 7750 3550
Text Notes 2650 1100 0    50   ~ 0
*Arduino 5V: max 1.2A out
Wire Wire Line
	4850 2450 5100 2450
$Comp
L Motor:Fan_4pin M3
U 1 1 5DA97CB0
P 10400 1300
F 0 "M3" H 10558 1396 50  0000 L CNN
F 1 "Juno_X" H 10400 1050 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10400 1310 50  0001 C CNN
F 3 "http://www.raijintek.com/en/products_detail.php?ProductID=55" H 10400 1310 50  0001 C CNN
	1    10400 1300
	1    0    0    -1  
$EndComp
Text Notes 9800 900  0    50   ~ 0
Cool side fan
Text GLabel 8400 1400 0    50   Input ~ 0
fpwm
Wire Wire Line
	7950 950  7950 1000
Wire Wire Line
	8400 1400 8500 1400
Wire Wire Line
	9700 1400 10100 1400
Text GLabel 9700 1400 0    50   Input ~ 0
fpwm2
Text GLabel 7500 1400 0    50   Input ~ 0
fpwm1
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R11
U 1 1 5FE19677
P 4400 2350
F 0 "R11" V 4400 2350 50  0000 C CNN
F 1 "150" V 4500 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4330 2350 50  0001 C CNN
F 3 "~" H 4400 2350 50  0001 C CNN
	1    4400 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3050 8100 3050
Wire Wire Line
	8100 3050 8100 3150
Wire Wire Line
	8100 3600 7750 3600
Connection ~ 7750 3600
Wire Wire Line
	8050 3150 8100 3150
Connection ~ 8100 3150
Wire Wire Line
	8100 3150 8100 3250
Wire Wire Line
	8100 3250 8050 3250
Connection ~ 8100 3250
Wire Wire Line
	8100 3250 8100 3350
Wire Wire Line
	8050 3350 8100 3350
Connection ~ 8100 3350
Wire Wire Line
	8100 3350 8100 3600
Wire Wire Line
	7850 5750 7900 5750
Wire Wire Line
	7900 5650 7850 5650
Wire Wire Line
	7850 5550 7900 5550
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R21
U 1 1 5D6E1EC1
P 7700 5650
F 0 "R21" V 7700 5650 50  0000 C CNN
F 1 "150" V 7584 5650 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5650 50  0001 C CNN
F 3 "~" H 7700 5650 50  0001 C CNN
	1    7700 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	10100 5950 10250 5950
Wire Wire Line
	10100 6050 10250 6050
Wire Wire Line
	7900 6250 7850 6250
Wire Wire Line
	7850 6150 7900 6150
Wire Wire Line
	7900 6050 7850 6050
Wire Wire Line
	7850 5950 7900 5950
Wire Wire Line
	7900 5850 7850 5850
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R20
U 1 1 5D6E0C24
P 7700 5550
F 0 "R20" V 7700 5550 50  0000 C CNN
F 1 "8x150" V 7600 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5550 50  0001 C CNN
F 3 "~" H 7700 5550 50  0001 C CNN
	1    7700 5550
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R22
U 1 1 5D6E231E
P 7700 5750
F 0 "R22" V 7700 5750 50  0000 C CNN
F 1 "150" V 7584 5750 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5750 50  0001 C CNN
F 3 "~" H 7700 5750 50  0001 C CNN
	1    7700 5750
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R23
U 1 1 5D6E2467
P 7700 5850
F 0 "R23" V 7700 5850 50  0000 C CNN
F 1 "150" V 7584 5850 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5850 50  0001 C CNN
F 3 "~" H 7700 5850 50  0001 C CNN
	1    7700 5850
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R24
U 1 1 5D6E2582
P 7700 5950
F 0 "R24" V 7700 5950 50  0000 C CNN
F 1 "150" V 7584 5950 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5950 50  0001 C CNN
F 3 "~" H 7700 5950 50  0001 C CNN
	1    7700 5950
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R25
U 1 1 5D6E2678
P 7700 6050
F 0 "R25" V 7700 6050 50  0000 C CNN
F 1 "150" V 7600 6050 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 6050 50  0001 C CNN
F 3 "~" H 7700 6050 50  0001 C CNN
	1    7700 6050
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R26
U 1 1 5D6E278D
P 7700 6150
F 0 "R26" V 7700 6150 50  0000 C CNN
F 1 "150" V 7584 6150 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 6150 50  0001 C CNN
F 3 "~" H 7700 6150 50  0001 C CNN
	1    7700 6150
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R27
U 1 1 5D6E28CC
P 7700 6250
F 0 "R27" V 7700 6250 50  0000 C CNN
F 1 "150" V 7584 6250 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 6250 50  0001 C CNN
F 3 "~" H 7700 6250 50  0001 C CNN
	1    7700 6250
	0    1    1    0   
$EndComp
$Comp
L cheesebox-rescue:CC56-12YWA-Display_Character-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue U6
U 1 1 5D6DEC4B
P 9000 5850
F 0 "U6" H 9000 6517 50  0000 C CNN
F 1 "LTC-4727JS" H 9000 6426 50  0000 C CNN
F 2 "Display_7Segment:CC56-12YWA" H 9000 5250 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTC-4727JS.pdf" H 8570 5880 50  0001 C CNN
	1    9000 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 6150 10250 6150
Wire Wire Line
	10100 6250 10250 6250
Wire Wire Line
	6150 2050 6100 2050
Wire Wire Line
	6250 2150 6100 2150
Text Notes 6200 5550 2    50   ~ 0
Display/mode switch
$Comp
L Device:R R15
U 1 1 60179C67
P 6450 5900
F 0 "R15" H 6400 5900 50  0000 R CNN
F 1 "4x10k" H 6500 6100 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 5900 50  0001 C CNN
F 3 "~" H 6450 5900 50  0001 C CNN
	1    6450 5900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 60187447
P 6450 6200
F 0 "R16" H 6400 6200 50  0000 R CNN
F 1 "4x10k" H 6500 6400 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 6200 50  0001 C CNN
F 3 "~" H 6450 6200 50  0001 C CNN
	1    6450 6200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 601AC53E
P 6450 6500
F 0 "R17" H 6400 6500 50  0000 R CNN
F 1 "8x10k" H 6150 6700 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 6500 50  0001 C CNN
F 3 "~" H 6450 6500 50  0001 C CNN
	1    6450 6500
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 601AC544
P 6450 6800
F 0 "R18" H 6400 6800 50  0000 R CNN
F 1 "4x10k" H 6500 7000 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 6800 50  0001 C CNN
F 3 "~" H 6450 6800 50  0001 C CNN
	1    6450 6800
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 601AC54A
P 6450 7100
F 0 "R19" H 6400 7100 50  0000 R CNN
F 1 "4x10k" H 6500 7300 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 7100 50  0001 C CNN
F 3 "~" H 6450 7100 50  0001 C CNN
	1    6450 7100
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 601796C6
P 6450 5600
F 0 "R14" H 6400 5600 50  0000 R CNN
F 1 "4x10k" H 6500 5800 50  0001 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 5600 50  0001 C CNN
F 3 "~" H 6450 5600 50  0001 C CNN
	1    6450 5600
	-1   0    0    -1  
$EndComp
$Comp
L cheesebox-rescue:SW_Rotary8-cheesebox-rescue SW3
U 1 1 60276155
P 5800 6200
F 0 "SW3" H 5700 6781 50  0000 C CNN
F 1 "SW_Rotary8" H 5700 6690 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 5600 6900 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/C200/DS-Serie%23LOR.pdf" H 5600 6900 50  0001 C CNN
	1    5800 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5DC49BEE
P 6450 5300
F 0 "R13" H 6400 5300 50  0000 R CNN
F 1 "7x4k7" H 6400 5400 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 5300 50  0001 C CNN
F 3 "~" H 6450 5300 50  0001 C CNN
	1    6450 5300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 6600 6250 6600
Wire Wire Line
	6250 6600 6250 7250
Wire Wire Line
	6250 7250 6450 7250
Wire Wire Line
	6200 6500 6300 6500
Wire Wire Line
	6300 6500 6300 6950
Wire Wire Line
	6300 6950 6450 6950
Connection ~ 6450 6950
Wire Wire Line
	6200 6400 6350 6400
Wire Wire Line
	6350 6400 6350 6650
Wire Wire Line
	6350 6650 6450 6650
Connection ~ 6450 6650
Wire Wire Line
	6200 6300 6350 6300
Wire Wire Line
	6350 6300 6350 6350
Wire Wire Line
	6350 6350 6450 6350
Connection ~ 6450 6350
Wire Wire Line
	6200 6100 6350 6100
Wire Wire Line
	6350 6100 6350 6050
Wire Wire Line
	6350 6050 6450 6050
Connection ~ 6450 6050
Wire Wire Line
	6200 6000 6350 6000
Wire Wire Line
	6350 6000 6350 5750
Wire Wire Line
	6350 5750 6450 5750
Connection ~ 6450 5750
Wire Wire Line
	6200 5900 6300 5900
Wire Wire Line
	6300 5900 6300 5450
Wire Wire Line
	6300 5450 6450 5450
Connection ~ 6450 5450
Wire Wire Line
	6200 5800 6250 5800
Wire Wire Line
	6250 5800 6250 5150
Wire Wire Line
	6250 5150 6450 5150
Wire Wire Line
	5150 6200 5200 6200
Text Notes 7700 2250 0    50   ~ 0
Digit display\nmultiplexer
$Comp
L power:GND #GND03
U 1 1 5D74BF3D
P 3050 3750
F 0 "#GND03" H 3050 3500 50  0001 C CNN
F 1 "GND" H 3050 3600 50  0000 C CNN
F 2 "" H 3050 3750 50  0001 C CNN
F 3 "" H 3050 3750 50  0001 C CNN
	1    3050 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:Peltier_Element PE1
U 1 1 5D73C7AF
P 1300 2300
F 0 "PE1" V 1350 2250 50  0000 R CNN
F 1 "Peltier" V 1250 2250 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1300 2230 50  0001 C CNN
F 3 "~" V 1300 2325 50  0001 C CNN
	1    1300 2300
	0    -1   -1   0   
$EndComp
$Comp
L cheesebox-rescue:IRF3205-Transistor_FET-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue Q1
U 1 1 5D6BC396
P 1400 2950
F 0 "Q1" H 1606 2996 50  0000 L CNN
F 1 "IRF3708PBF" H 1606 2905 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 1650 2875 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 1400 2950 50  0001 L CNN
	1    1400 2950
	-1   0    0    -1  
$EndComp
Connection ~ 6450 7250
Connection ~ 6450 5150
Wire Wire Line
	6450 5050 6450 5150
Wire Wire Line
	6450 7300 6450 7250
Text Notes 8550 5300 2    50   ~ 0
Digit display
$Comp
L power:+12V #PWR12V02
U 1 1 5FF71F15
P 3050 2900
F 0 "#PWR12V02" H 3050 2750 50  0001 C CNN
F 1 "+12V" H 3065 3073 50  0000 C CNN
F 2 "" H 3050 2900 50  0001 C CNN
F 3 "" H 3050 2900 50  0001 C CNN
	1    3050 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5F13AA83
P 3750 3100
F 0 "R8" H 3820 3146 50  0000 L CNN
F 1 "10k" H 3820 3055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3680 3100 50  0001 C CNN
F 3 "~" H 3750 3100 50  0001 C CNN
	1    3750 3100
	1    0    0    -1  
$EndComp
Text Notes 650  4250 0    50   ~ 10
Temperature sensors
$Comp
L Device:R R2
U 1 1 60047C60
P 2850 3450
F 0 "R2" V 2750 3350 50  0000 L CNN
F 1 "100" V 2950 3350 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2780 3450 50  0001 C CNN
F 3 "~" H 2850 3450 50  0001 C CNN
	1    2850 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 60087B7B
P 3050 3600
F 0 "R4" H 2900 3600 50  0000 L CNN
F 1 "10k" H 2850 3700 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2980 3600 50  0001 C CNN
F 3 "~" H 3050 3600 50  0001 C CNN
	1    3050 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3000 3450 3050 3450
Wire Wire Line
	3050 2950 3750 2950
Wire Wire Line
	3050 3050 3050 2950
Wire Wire Line
	3400 3250 3350 3250
$Comp
L Device:R R6
U 1 1 60119696
P 3550 3250
F 0 "R6" V 3450 3200 50  0000 L CNN
F 1 "10k" V 3650 3200 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 3250 50  0001 C CNN
F 3 "~" H 3550 3250 50  0001 C CNN
	1    3550 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 3250 3750 3250
Connection ~ 3750 3250
Connection ~ 3050 3450
$Comp
L Device:Q_PNP_CBE Q2
U 1 1 6002F67F
P 3150 3250
F 0 "Q2" H 3050 3400 50  0000 L CNN
F 1 "BC557B" H 2850 3500 50  0000 L CNN
F 2 "" H 3350 3350 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/ON%20Semiconductor%20PDFs/BC556,%20557,%20558%20Rev2.pdf" H 3150 3250 50  0001 C CNN
	1    3150 3250
	-1   0    0    1   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R28
U 1 1 5FFD04BC
P 8550 2150
F 0 "R28" V 8600 2100 50  0000 L TNN
F 1 "1k" V 8450 2150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8480 2150 50  0001 C CNN
F 3 "~" H 8550 2150 50  0001 C CNN
	1    8550 2150
	0    1    -1   0   
$EndComp
$Comp
L cheesebox-rescue:S8050-Transistor_BJT-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue Q4
U 1 1 5FFD04B6
P 8950 2150
F 0 "Q4" H 9250 2300 50  0000 R CNN
F 1 "MPS650" H 9450 2200 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 9150 2075 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MPS650-D.PDF" H 8950 2150 50  0001 L CNN
	1    8950 2150
	1    0    0    -1  
$EndComp
Text GLabel 10250 5950 2    50   Input ~ 0
cc1
Text GLabel 10250 6050 2    50   Input ~ 0
cc2
Text GLabel 10250 6150 2    50   Input ~ 0
cc3
Text GLabel 10250 6250 2    50   Input ~ 0
cc4
Wire Wire Line
	7550 5550 7500 5550
Wire Wire Line
	7550 5650 7400 5650
Wire Wire Line
	7300 5750 7550 5750
Wire Wire Line
	6100 2850 7350 2850
Wire Wire Line
	6100 2950 7350 2950
Wire Wire Line
	1300 2500 1300 2600
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 6052C79E
P 750 550
F 0 "J1" V 812 594 50  0000 L CNN
F 1 "12VDC" V 903 594 50  0000 L CNN
F 2 "" H 750 550 50  0001 C CNN
F 3 "~" H 750 550 50  0001 C CNN
	1    750  550 
	0    1    1    0   
$EndComp
Text GLabel 1750 1250 2    50   Input ~ 0
a12v
Text GLabel 1700 3300 2    50   Input ~ 0
agnd
Wire Wire Line
	750  1250 1300 1250
Wire Wire Line
	750  750  750  1250
Connection ~ 1300 1250
Wire Wire Line
	1600 2950 1700 2950
Wire Wire Line
	1300 1250 1750 1250
Text GLabel 5950 800  1    50   Input ~ 0
f12v
Wire Wire Line
	5700 1150 5700 1200
Text GLabel 7500 1550 0    50   Input ~ 0
fgnd
Text GLabel 7500 950  0    50   Input ~ 0
f12v
Text GLabel 4550 800  1    50   Input ~ 0
eled3
Wire Wire Line
	4550 800  4550 2650
Connection ~ 4550 2650
Wire Wire Line
	4550 2650 5100 2650
Wire Wire Line
	10400 950  10400 1000
Wire Wire Line
	10400 1550 10400 1500
Text GLabel 1700 2950 2    50   Input ~ 0
gate
Text GLabel 10950 1900 2    50   Input ~ 0
cc1
Text GLabel 6800 4550 1    50   Input ~ 0
dp
Text GLabel 6900 4550 1    50   Input ~ 0
g
Text GLabel 7000 4550 1    50   Input ~ 0
f
Text GLabel 7100 4550 1    50   Input ~ 0
e
Text GLabel 7200 4550 1    50   Input ~ 0
d
Text GLabel 7300 4550 1    50   Input ~ 0
c
Text GLabel 7400 4550 1    50   Input ~ 0
b
Text GLabel 7500 4550 1    50   Input ~ 0
a
Text GLabel 6850 800  1    50   Input ~ 0
dp
Text GLabel 6750 800  1    50   Input ~ 0
g
Text GLabel 6650 800  1    50   Input ~ 0
f
Text GLabel 6550 800  1    50   Input ~ 0
e
Text GLabel 6450 800  1    50   Input ~ 0
d
Text GLabel 6350 800  1    50   Input ~ 0
c
Text GLabel 6250 800  1    50   Input ~ 0
b
Text GLabel 6150 800  1    50   Input ~ 0
a
Text GLabel 5200 800  1    50   Input ~ 0
e5v
Text GLabel 5700 4000 3    50   Input ~ 0
egnd
Text GLabel 5800 4000 3    50   Input ~ 0
fgnd
Wire Wire Line
	5400 1150 5400 1250
Text GLabel 3500 4600 1    50   Input ~ 0
egnd
Text GLabel 5850 800  1    50   Input ~ 0
e12v
Text GLabel 1700 2000 2    50   Input ~ 0
e12vpel
Text GLabel 1700 2600 2    50   Input ~ 0
eled2
Text GLabel 4850 800  1    50   Input ~ 0
pot
Text GLabel 3350 4550 1    50   Input ~ 0
e5v
Text GLabel 2550 2400 0    50   Input ~ 0
sw1
Text GLabel 5150 4550 1    50   Input ~ 0
sw1
Text GLabel 1500 4900 2    50   Input ~ 0
tdq
Text GLabel 5300 800  1    50   Input ~ 0
t5v
Text GLabel 5600 4000 3    50   Input ~ 0
tgnd
Text GLabel 1500 4450 2    50   Input ~ 0
t5v
Text GLabel 1500 5250 2    50   Input ~ 0
tgnd
$Comp
L cheesebox-rescue:DS18B20-Sensor_Temperature-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue U1
U 1 1 5F46172A
P 900 4900
F 0 "U1" H 900 5250 50  0000 L CNN
F 1 "DS18B20" H 900 5150 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H -100 4650 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 750 5150 50  0001 C CNN
	1    900  4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2000 1300 2000
Connection ~ 1300 2000
Wire Wire Line
	1300 2000 1300 2100
Wire Wire Line
	1700 2600 1300 2600
Connection ~ 1300 2600
Wire Wire Line
	1300 2600 1300 2750
Text Notes 900  900  0    50   ~ 10
Off-board Heating
Text GLabel 2550 3450 0    50   Input ~ 0
gate
$Comp
L power:+12V #PWR12V01
U 1 1 60E98303
P 1300 1200
F 0 "#PWR12V01" H 1300 1050 50  0001 C CNN
F 1 "+12V" H 1315 1373 50  0000 C CNN
F 2 "" H 1300 1200 50  0001 C CNN
F 3 "" H 1300 1200 50  0001 C CNN
	1    1300 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1200 1300 1250
Wire Notes Line
	1650 3550 1650 800 
Wire Notes Line
	1650 800  550  800 
Wire Notes Line
	550  800  550  3550
Wire Notes Line
	550  3550 1650 3550
$Comp
L power:GND #GND01
U 1 1 60F84868
P 1300 3350
F 0 "#GND01" H 1300 3100 50  0001 C CNN
F 1 "GND" H 1300 3200 50  0000 C CNN
F 2 "" H 1300 3350 50  0001 C CNN
F 3 "" H 1300 3350 50  0001 C CNN
	1    1300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 3350 1300 3300
Wire Wire Line
	1300 3300 1700 3300
Wire Wire Line
	1300 3150 1300 3300
Connection ~ 1300 3300
Wire Wire Line
	650  750  650  3300
Wire Wire Line
	650  3300 1300 3300
Text Label 7300 2650 0    50   ~ 0
A
$Comp
L power:+5V #PWR12V08
U 1 1 61171EF9
P 5400 1150
F 0 "#PWR12V08" H 5400 1000 50  0001 C CNN
F 1 "+5V" H 5415 1323 50  0000 C CNN
F 2 "" H 5400 1150 50  0001 C CNN
F 3 "" H 5400 1150 50  0001 C CNN
	1    5400 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR12V011
U 1 1 6117A20F
P 7650 2000
F 0 "#PWR12V011" H 7650 1850 50  0001 C CNN
F 1 "+5V" H 7750 2100 50  0000 C CNN
F 2 "" H 7650 2000 50  0001 C CNN
F 3 "" H 7650 2000 50  0001 C CNN
	1    7650 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR12V09
U 1 1 6132D3D5
P 5700 1150
F 0 "#PWR12V09" H 5700 1000 50  0001 C CNN
F 1 "+12V" H 5715 1323 50  0000 C CNN
F 2 "" H 5700 1150 50  0001 C CNN
F 3 "" H 5700 1150 50  0001 C CNN
	1    5700 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 1200 5850 800 
Wire Wire Line
	5700 1200 5850 1200
Connection ~ 5700 1200
Wire Wire Line
	5700 1200 5700 1450
Wire Wire Line
	5300 1250 5400 1250
Connection ~ 5400 1250
Wire Wire Line
	6150 2050 6150 800 
Wire Wire Line
	6250 800  6250 2150
Wire Wire Line
	6350 2250 6350 800 
Wire Wire Line
	6100 2250 6350 2250
Wire Wire Line
	6450 800  6450 2350
Wire Wire Line
	6100 2350 6450 2350
Wire Wire Line
	6550 2450 6550 800 
Wire Wire Line
	6100 2450 6550 2450
Wire Wire Line
	6650 800  6650 2550
Wire Wire Line
	6100 2550 6650 2550
Wire Wire Line
	6750 800  6750 2650
Wire Wire Line
	6100 2650 6750 2650
Wire Wire Line
	6850 800  6850 2750
Wire Wire Line
	6100 2750 6850 2750
$Comp
L power:GND #GND010
U 1 1 6157008F
P 5500 3650
F 0 "#GND010" H 5500 3400 50  0001 C CNN
F 1 "GND" H 5500 3500 50  0000 C CNN
F 2 "" H 5500 3650 50  0001 C CNN
F 3 "" H 5500 3650 50  0001 C CNN
	1    5500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5550 7500 4550
Wire Wire Line
	7400 5650 7400 4550
Wire Wire Line
	7300 4550 7300 5750
Wire Wire Line
	7200 5850 7200 4550
Wire Wire Line
	7200 5850 7550 5850
Wire Wire Line
	7100 4550 7100 5950
Wire Wire Line
	7100 5950 7550 5950
Wire Wire Line
	7000 4550 7000 6050
Wire Wire Line
	7000 6050 7550 6050
Wire Wire Line
	6900 4550 6900 6150
Wire Wire Line
	6900 6150 7550 6150
Wire Wire Line
	6800 4550 6800 6250
Wire Wire Line
	6800 6250 7550 6250
$Comp
L power:+5V #PWR12V010
U 1 1 61695466
P 6450 5050
F 0 "#PWR12V010" H 6450 4900 50  0001 C CNN
F 1 "+5V" H 6465 5223 50  0000 C CNN
F 2 "" H 6450 5050 50  0001 C CNN
F 3 "" H 6450 5050 50  0001 C CNN
	1    6450 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR12V03
U 1 1 616970E1
P 3350 4700
F 0 "#PWR12V03" H 3350 4550 50  0001 C CNN
F 1 "+5V" H 3365 4873 50  0000 C CNN
F 2 "" H 3350 4700 50  0001 C CNN
F 3 "" H 3350 4700 50  0001 C CNN
	1    3350 4700
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR12V06
U 1 1 616E9DA0
P 3950 6100
F 0 "#PWR12V06" H 3950 5950 50  0001 C CNN
F 1 "+5V" H 3965 6273 50  0000 C CNN
F 2 "" H 3950 6100 50  0001 C CNN
F 3 "" H 3950 6100 50  0001 C CNN
	1    3950 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND011
U 1 1 616ECEB1
P 6450 7300
F 0 "#GND011" H 6450 7050 50  0001 C CNN
F 1 "GND" H 6550 7200 50  0000 C CNN
F 2 "" H 6450 7300 50  0001 C CNN
F 3 "" H 6450 7300 50  0001 C CNN
	1    6450 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND06
U 1 1 616F73D3
P 3950 6500
F 0 "#GND06" H 3950 6250 50  0001 C CNN
F 1 "GND" H 3950 6350 50  0000 C CNN
F 2 "" H 3950 6500 50  0001 C CNN
F 3 "" H 3950 6500 50  0001 C CNN
	1    3950 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND04
U 1 1 61706ABD
P 3500 4700
F 0 "#GND04" H 3500 4450 50  0001 C CNN
F 1 "GND" H 3500 4550 50  0000 C CNN
F 2 "" H 3500 4700 50  0001 C CNN
F 3 "" H 3500 4700 50  0001 C CNN
	1    3500 4700
	1    0    0    -1  
$EndComp
Text GLabel 3350 7500 3    50   Input ~ 0
eled3
$Comp
L power:+5V #PWR12V04
U 1 1 6169B3B8
P 3350 5550
F 0 "#PWR12V04" H 3350 5400 50  0001 C CNN
F 1 "+5V" H 3365 5723 50  0000 C CNN
F 2 "" H 3350 5550 50  0001 C CNN
F 3 "" H 3350 5550 50  0001 C CNN
	1    3350 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6300 4150 6300
Wire Wire Line
	5150 4550 5150 6200
Wire Wire Line
	3350 7500 3350 6450
Wire Wire Line
	3350 5750 3350 5550
Wire Wire Line
	3350 6150 3350 6050
Text Notes 3350 6200 0    50   ~ 0
FAN PWM\n
$Comp
L Device:R R5
U 1 1 5F7B0CA0
P 3350 5900
F 0 "R5" H 3200 5950 50  0000 L CNN
F 1 "470" H 3150 5850 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3280 5900 50  0001 C CNN
F 3 "~" H 3350 5900 50  0001 C CNN
	1    3350 5900
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5F7B03DB
P 3350 6300
F 0 "D3" V 3400 6200 50  0000 R CNN
F 1 "LED" V 3300 6200 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3350 6300 50  0001 C CNN
F 3 "~" H 3350 6300 50  0001 C CNN
	1    3350 6300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F7B1474
P 2550 5900
F 0 "R1" H 2620 5946 50  0000 L CNN
F 1 "1k47" H 2620 5855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2480 5900 50  0001 C CNN
F 3 "~" H 2550 5900 50  0001 C CNN
	1    2550 5900
	1    0    0    -1  
$EndComp
Text Notes 2550 6200 0    50   ~ 0
PWR ON\n
Wire Wire Line
	2550 6050 2550 6150
Wire Wire Line
	2550 6450 2550 6550
$Comp
L Device:LED D1
U 1 1 5F7B1BEA
P 2550 6300
F 0 "D1" V 2589 6183 50  0000 R CNN
F 1 "LED" V 2498 6183 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 2550 6300 50  0001 C CNN
F 3 "~" H 2550 6300 50  0001 C CNN
	1    2550 6300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #GND02
U 1 1 617076AF
P 2550 6550
F 0 "#GND02" H 2550 6300 50  0001 C CNN
F 1 "GND" H 2550 6400 50  0000 C CNN
F 2 "" H 2550 6550 50  0001 C CNN
F 3 "" H 2550 6550 50  0001 C CNN
	1    2550 6550
	1    0    0    -1  
$EndComp
Text GLabel 2950 7500 3    50   Input ~ 0
eled2
Wire Wire Line
	2950 6450 2950 7500
Text Notes 2950 6200 0    50   ~ 0
HEAT ON\n
$Comp
L Device:LED D2
U 1 1 5DA78263
P 2950 6300
F 0 "D2" V 2989 6183 50  0000 R CNN
F 1 "LED" V 2898 6183 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 2950 6300 50  0001 C CNN
F 3 "~" H 2950 6300 50  0001 C CNN
	1    2950 6300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5DA7DD91
P 2950 5900
F 0 "R3" H 3020 5946 50  0000 L CNN
F 1 "1k47" H 3020 5855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2880 5900 50  0001 C CNN
F 3 "~" H 2950 5900 50  0001 C CNN
	1    2950 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 6050 2950 6150
Wire Notes Line
	6850 7450 6850 6450
Wire Notes Line
	6850 6450 10200 6450
Wire Notes Line
	10200 6450 10200 4600
Text Notes 2400 4700 0    50   ~ 10
UI Panel
Wire Wire Line
	5500 3600 5500 3650
Connection ~ 5500 3600
Wire Wire Line
	5500 3600 5600 3600
Wire Wire Line
	7700 3600 7750 3600
Connection ~ 7700 3600
Wire Wire Line
	7650 3600 7700 3600
Wire Wire Line
	7700 3600 7700 3650
$Comp
L power:GND #GND013
U 1 1 5FEB208D
P 7700 3650
F 0 "#GND013" H 7700 3400 50  0001 C CNN
F 1 "GND" H 7700 3500 50  0000 C CNN
F 2 "" H 7700 3650 50  0001 C CNN
F 3 "" H 7700 3650 50  0001 C CNN
	1    7700 3650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5FF25CA8
P 1300 1600
F 0 "SW1" V 1346 1512 50  0000 R CNN
F 1 "SW_SPST" V 1255 1512 50  0000 R CNN
F 2 "" H 1300 1600 50  0001 C CNN
F 3 "~" H 1300 1600 50  0001 C CNN
	1    1300 1600
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_DPDT_x2 SW2
U 1 1 5FF24592
P 4650 6350
F 0 "SW2" H 4650 6635 50  0000 C CNN
F 1 "SW_DPDT_x2" H 4650 6544 50  0000 C CNN
F 2 "" H 4650 6350 50  0001 C CNN
F 3 "~" H 4650 6350 50  0001 C CNN
	1    4650 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1250 1300 1400
Wire Wire Line
	1300 1800 1300 2000
Text Notes 5050 5900 2    50   ~ 0
Run/Set switch
Wire Wire Line
	3050 2900 3050 2950
Connection ~ 3050 2950
Text GLabel 10950 2950 2    50   Input ~ 0
cc4
Text GLabel 10950 2850 2    50   Input ~ 0
cc3
Text GLabel 10950 2000 2    50   Input ~ 0
cc2
Text GLabel 2550 3350 0    50   Input ~ 0
a12v
Text GLabel 2550 3550 0    50   Input ~ 0
agnd
Wire Wire Line
	8700 2150 8750 2150
$Comp
L power:GND #GND014
U 1 1 60540FDC
P 9050 2500
F 0 "#GND014" H 9050 2250 50  0001 C CNN
F 1 "GND" H 9050 2350 50  0000 C CNN
F 2 "" H 9050 2500 50  0001 C CNN
F 3 "" H 9050 2500 50  0001 C CNN
	1    9050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2500 9050 2450
Wire Wire Line
	8050 2650 8350 2650
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R29
U 1 1 5FFD04D0
P 8550 2450
F 0 "R29" V 8650 2450 50  0000 C CNN
F 1 "10k" V 8450 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8480 2450 50  0001 C CNN
F 3 "~" H 8550 2450 50  0001 C CNN
	1    8550 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8350 2650 8350 2450
Wire Wire Line
	8350 2150 8400 2150
Wire Wire Line
	8700 2450 9050 2450
Connection ~ 9050 2450
Wire Wire Line
	9050 2450 9050 2350
Wire Wire Line
	8400 2450 8350 2450
Connection ~ 8350 2450
Wire Wire Line
	8350 2450 8350 2150
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R32
U 1 1 60966D67
P 9550 2250
F 0 "R32" V 9600 2200 50  0000 L TNN
F 1 "1k" V 9450 2250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9480 2250 50  0001 C CNN
F 3 "~" H 9550 2250 50  0001 C CNN
	1    9550 2250
	0    1    -1   0   
$EndComp
$Comp
L cheesebox-rescue:S8050-Transistor_BJT-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue Q6
U 1 1 60966D6D
P 9950 2250
F 0 "Q6" H 10250 2400 50  0000 R CNN
F 1 "MPS650" H 10450 2300 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 10150 2175 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MPS650-D.PDF" H 9950 2250 50  0001 L CNN
	1    9950 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2250 9750 2250
$Comp
L power:GND #GND016
U 1 1 60966D74
P 10050 2600
F 0 "#GND016" H 10050 2350 50  0001 C CNN
F 1 "GND" H 10050 2450 50  0000 C CNN
F 2 "" H 10050 2600 50  0001 C CNN
F 3 "" H 10050 2600 50  0001 C CNN
	1    10050 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2600 10050 2550
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R33
U 1 1 60966D7B
P 9550 2550
F 0 "R33" V 9650 2550 50  0000 C CNN
F 1 "10k" V 9450 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9480 2550 50  0001 C CNN
F 3 "~" H 9550 2550 50  0001 C CNN
	1    9550 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9350 2250 9400 2250
Wire Wire Line
	9700 2550 10050 2550
Connection ~ 10050 2550
Wire Wire Line
	10050 2550 10050 2450
Wire Wire Line
	9400 2550 9350 2550
Connection ~ 9350 2550
Wire Wire Line
	9350 2550 9350 2250
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R30
U 1 1 60979FD4
P 8600 3100
F 0 "R30" V 8650 3050 50  0000 L TNN
F 1 "1k" V 8500 3100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8530 3100 50  0001 C CNN
F 3 "~" H 8600 3100 50  0001 C CNN
	1    8600 3100
	0    1    -1   0   
$EndComp
$Comp
L cheesebox-rescue:S8050-Transistor_BJT-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue Q5
U 1 1 60979FDA
P 9000 3100
F 0 "Q5" H 9300 3250 50  0000 R CNN
F 1 "MPS650" H 9500 3150 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 9200 3025 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MPS650-D.PDF" H 9000 3100 50  0001 L CNN
	1    9000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3100 8800 3100
$Comp
L power:GND #GND015
U 1 1 60979FE1
P 9100 3450
F 0 "#GND015" H 9100 3200 50  0001 C CNN
F 1 "GND" H 9100 3300 50  0000 C CNN
F 2 "" H 9100 3450 50  0001 C CNN
F 3 "" H 9100 3450 50  0001 C CNN
	1    9100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 3450 9100 3400
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R31
U 1 1 60979FE8
P 8600 3400
F 0 "R31" V 8700 3400 50  0000 C CNN
F 1 "10k" V 8500 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8530 3400 50  0001 C CNN
F 3 "~" H 8600 3400 50  0001 C CNN
	1    8600 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 3100 8450 3100
Wire Wire Line
	8750 3400 9100 3400
Connection ~ 9100 3400
Wire Wire Line
	9100 3400 9100 3300
Wire Wire Line
	8450 3400 8400 3400
Wire Wire Line
	8400 3400 8400 3100
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R34
U 1 1 6098DC39
P 9600 3200
F 0 "R34" V 9650 3150 50  0000 L TNN
F 1 "1k" V 9500 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9530 3200 50  0001 C CNN
F 3 "~" H 9600 3200 50  0001 C CNN
	1    9600 3200
	0    1    -1   0   
$EndComp
$Comp
L cheesebox-rescue:S8050-Transistor_BJT-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue Q7
U 1 1 6098DC3F
P 10000 3200
F 0 "Q7" H 10300 3350 50  0000 R CNN
F 1 "MPS650" H 10500 3250 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 10200 3125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MPS650-D.PDF" H 10000 3200 50  0001 L CNN
	1    10000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 3200 9800 3200
$Comp
L power:GND #GND017
U 1 1 6098DC46
P 10100 3550
F 0 "#GND017" H 10100 3300 50  0001 C CNN
F 1 "GND" H 10100 3400 50  0000 C CNN
F 2 "" H 10100 3550 50  0001 C CNN
F 3 "" H 10100 3550 50  0001 C CNN
	1    10100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 3550 10100 3500
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R35
U 1 1 6098DC4D
P 9600 3500
F 0 "R35" V 9700 3500 50  0000 C CNN
F 1 "10k" V 9500 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9530 3500 50  0001 C CNN
F 3 "~" H 9600 3500 50  0001 C CNN
	1    9600 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 3200 9450 3200
Wire Wire Line
	9750 3500 10100 3500
Connection ~ 10100 3500
Wire Wire Line
	10100 3500 10100 3400
Wire Wire Line
	9450 3500 9400 3500
Wire Wire Line
	9400 3500 9400 3200
$Comp
L power:GND #GND012
U 1 1 609F66C0
P 7450 2350
F 0 "#GND012" H 7450 2100 50  0001 C CNN
F 1 "GND" H 7450 2200 50  0000 C CNN
F 2 "" H 7450 2350 50  0001 C CNN
F 3 "" H 7450 2350 50  0001 C CNN
	1    7450 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 60A1BDBD
P 7450 2200
F 0 "C4" H 7250 2200 50  0000 L CNN
F 1 "100n" H 7250 2100 50  0000 L CNN
F 2 "" H 7450 2200 50  0001 C CNN
F 3 "~" H 7450 2200 50  0001 C CNN
	1    7450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2100 7450 2050
Wire Wire Line
	7450 2300 7450 2350
Wire Wire Line
	8050 2750 9350 2750
Wire Wire Line
	9350 2550 9350 2750
Wire Wire Line
	7650 2050 7650 2450
Wire Wire Line
	7450 2050 7650 2050
Wire Wire Line
	7650 2050 7650 2000
Connection ~ 7650 2050
Wire Wire Line
	8050 2850 8400 2850
Wire Wire Line
	8400 2850 8400 3100
Connection ~ 8400 3100
Wire Wire Line
	9400 3700 9400 3500
Connection ~ 9400 3500
Wire Wire Line
	9400 3700 8200 3700
Wire Wire Line
	8200 3700 8200 2950
Wire Wire Line
	8200 2950 8050 2950
Wire Wire Line
	9050 1950 9050 1900
Wire Wire Line
	9050 1900 10950 1900
Wire Wire Line
	10050 2050 10050 2000
Wire Wire Line
	10050 2000 10950 2000
Wire Wire Line
	9100 2900 9100 2850
Wire Wire Line
	9100 2850 10950 2850
Wire Wire Line
	10100 3000 10100 2950
Wire Wire Line
	10100 2950 10950 2950
NoConn ~ 5100 1850
NoConn ~ 6100 1850
NoConn ~ 6100 1950
NoConn ~ 6100 3050
NoConn ~ 6100 3150
NoConn ~ 5100 1950
NoConn ~ 5100 2250
Text GLabel 2550 1600 0    50   Input ~ 0
sw2
Wire Wire Line
	5400 1250 5400 1450
Wire Notes Line
	10900 1800 6950 1800
Wire Notes Line
	6950 1800 6950 850 
Wire Notes Line
	6950 850  2600 850 
Wire Notes Line
	2600 850  2600 3950
Wire Notes Line
	2600 3950 10900 3950
Wire Notes Line
	10900 3950 10900 1800
Text Notes 2650 950  0    50   ~ 10
Arduino Control Board
Wire Wire Line
	5950 800  5950 1200
Wire Wire Line
	5950 1200 5850 1200
Connection ~ 5850 1200
Wire Wire Line
	5300 800  5300 1250
Wire Wire Line
	5300 1250 5200 1250
Wire Wire Line
	5200 800  5200 1250
Connection ~ 5300 1250
Text Notes 5500 1000 0    50   ~ 0
*
Wire Wire Line
	5600 4000 5600 3600
Connection ~ 5600 3600
Wire Wire Line
	5600 3600 5700 3600
Wire Wire Line
	5800 3600 5800 4000
Wire Wire Line
	5700 4000 5700 3600
Connection ~ 5700 3600
Wire Wire Line
	5700 3600 5800 3600
$Comp
L Device:C_Small C2
U 1 1 615CF9EF
P 4050 1750
F 0 "C2" H 4050 1650 50  0000 L CNN
F 1 "100n" H 4050 1850 50  0000 L CNN
F 2 "" H 4050 1750 50  0001 C CNN
F 3 "~" H 4050 1750 50  0001 C CNN
	1    4050 1750
	1    0    0    1   
$EndComp
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R9
U 1 1 616E480E
P 3850 1600
F 0 "R9" V 3750 1600 50  0000 C CNN
F 1 "1k" V 3950 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3780 1600 50  0001 C CNN
F 3 "~" H 3850 1600 50  0001 C CNN
	1    3850 1600
	0    1    1    0   
$EndComp
$Comp
L power:GND #GND07
U 1 1 617625A9
P 4050 1900
F 0 "#GND07" H 4050 1650 50  0001 C CNN
F 1 "GND" H 4050 1750 50  0000 C CNN
F 2 "" H 4050 1900 50  0001 C CNN
F 3 "" H 4050 1900 50  0001 C CNN
	1    4050 1900
	1    0    0    -1  
$EndComp
Text GLabel 4400 4550 1    50   Input ~ 0
sw2
Wire Wire Line
	1200 4900 1500 4900
Wire Notes Line
	1300 4400 650  4400
Wire Notes Line
	650  5300 1300 5300
Wire Wire Line
	900  4600 900  4450
Wire Wire Line
	900  4450 1500 4450
Wire Wire Line
	900  5200 900  5250
Wire Wire Line
	900  5250 1500 5250
Wire Notes Line
	1300 4400 1300 5300
Wire Notes Line
	650  4400 650  5300
Text GLabel 1500 5900 2    50   Input ~ 0
tdq
Text GLabel 1500 5450 2    50   Input ~ 0
t5v
Text GLabel 1500 6250 2    50   Input ~ 0
tgnd
$Comp
L cheesebox-rescue:DS18B20-Sensor_Temperature-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue U2
U 1 1 61B282A1
P 900 5900
F 0 "U2" H 900 6250 50  0000 L CNN
F 1 "DS18B20" H 900 6150 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H -100 5650 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 750 6150 50  0001 C CNN
	1    900  5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5900 1500 5900
Wire Notes Line
	1300 5400 650  5400
Wire Notes Line
	650  6300 1300 6300
Wire Wire Line
	900  5600 900  5450
Wire Wire Line
	900  5450 1500 5450
Wire Wire Line
	900  6200 900  6250
Wire Wire Line
	900  6250 1500 6250
Wire Notes Line
	1300 5400 1300 6300
Wire Notes Line
	650  5400 650  6300
Text GLabel 1500 6900 2    50   Input ~ 0
tdq
Text GLabel 1500 6450 2    50   Input ~ 0
t5v
Text GLabel 1500 7250 2    50   Input ~ 0
tgnd
$Comp
L cheesebox-rescue:DS18B20-Sensor_Temperature-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue U3
U 1 1 61B41FE1
P 900 6900
F 0 "U3" H 900 7250 50  0000 L CNN
F 1 "DS18B20" H 900 7150 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H -100 6650 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 750 7150 50  0001 C CNN
	1    900  6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6900 1500 6900
Wire Notes Line
	1300 6400 650  6400
Wire Notes Line
	650  7300 1300 7300
Wire Wire Line
	900  6600 900  6450
Wire Wire Line
	900  6450 1500 6450
Wire Wire Line
	900  7200 900  7250
Wire Wire Line
	900  7250 1500 7250
Wire Notes Line
	1300 6400 1300 7300
Wire Notes Line
	650  6400 650  7300
Text Notes 650  4350 0    50   ~ 0
(ambient, inside, water)
Wire Wire Line
	4150 4550 4150 6300
Wire Wire Line
	2950 4550 2950 5750
Text GLabel 4150 4550 1    50   Input ~ 0
pot
Text GLabel 2950 4600 1    50   Input ~ 0
e12vpel
Text GLabel 2800 4600 1    50   Input ~ 0
e12v
Wire Wire Line
	4400 4550 4400 6350
Wire Wire Line
	4400 6350 4450 6350
$Comp
L power:+5V #PWR12V07
U 1 1 61DE4AFE
P 4950 6100
F 0 "#PWR12V07" H 4950 5950 50  0001 C CNN
F 1 "+5V" H 4965 6273 50  0000 C CNN
F 2 "" H 4950 6100 50  0001 C CNN
F 3 "" H 4950 6100 50  0001 C CNN
	1    4950 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND09
U 1 1 61DE5339
P 4950 6550
F 0 "#GND09" H 4950 6300 50  0001 C CNN
F 1 "GND" H 4950 6400 50  0000 C CNN
F 2 "" H 4950 6550 50  0001 C CNN
F 3 "" H 4950 6550 50  0001 C CNN
	1    4950 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6250 4950 6250
Wire Wire Line
	4950 6250 4950 6100
Wire Wire Line
	4850 6450 4950 6450
Wire Wire Line
	4950 6450 4950 6550
$Comp
L power:GND #GND08
U 1 1 61E2566F
P 4950 1650
F 0 "#GND08" H 4950 1400 50  0001 C CNN
F 1 "GND" H 4950 1500 50  0000 C CNN
F 2 "" H 4950 1650 50  0001 C CNN
F 3 "" H 4950 1650 50  0001 C CNN
	1    4950 1650
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 61E2501E
P 4950 1500
F 0 "C3" H 5000 1600 50  0000 L CNN
F 1 "100n" H 4950 1400 50  0000 L CNN
F 2 "" H 4950 1500 50  0001 C CNN
F 3 "~" H 4950 1500 50  0001 C CNN
	1    4950 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1400 4950 1350
Wire Wire Line
	4950 1350 4850 1350
Wire Wire Line
	4950 1650 4950 1600
Wire Notes Line
	2350 7450 2350 4600
Wire Notes Line
	2350 7450 6850 7450
Wire Notes Line
	2350 4600 10200 4600
Wire Wire Line
	3350 4550 3350 4700
Wire Wire Line
	3500 4700 3500 4550
Wire Wire Line
	2550 5750 2550 5600
Wire Wire Line
	2550 5600 2800 5600
Wire Wire Line
	2800 5600 2800 4550
NoConn ~ 10100 5450
NoConn ~ 10100 5550
NoConn ~ 10100 5650
NoConn ~ 10100 5850
Wire Wire Line
	7500 1400 7650 1400
Wire Wire Line
	7950 1500 7950 1550
Connection ~ 7950 1550
Wire Wire Line
	7500 1550 7950 1550
Wire Wire Line
	7950 1550 8800 1550
Wire Wire Line
	8800 1550 8800 1500
Wire Wire Line
	8800 950  8800 1000
Connection ~ 7950 950 
Wire Wire Line
	7950 950  8800 950 
$Comp
L Motor:Fan_4pin M2
U 1 1 5DA96A88
P 8800 1300
F 0 "M2" H 8958 1396 50  0000 L CNN
F 1 "2xArctic_P12" H 8100 1100 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8800 1310 50  0001 C CNN
F 3 "http://www.formfactors.org/developer%5Cspecs%5Crev1_2_public.pdf" H 8800 1310 50  0001 C CNN
	1    8800 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 950  7950 950 
$Comp
L Motor:Fan_4pin M1
U 1 1 5DA95263
P 7950 1300
F 0 "M1" H 8100 1400 50  0000 L CNN
F 1 "Arctic_P12" H 8000 1500 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7950 1310 50  0001 C CNN
F 3 "http://www.formfactors.org/developer%5Cspecs%5Crev1_2_public.pdf" H 7950 1310 50  0001 C CNN
	1    7950 1300
	1    0    0    -1  
$EndComp
Text GLabel 9700 950  0    50   Input ~ 0
f12v
Text GLabel 9700 1550 0    50   Input ~ 0
fgnd
Wire Wire Line
	9700 950  10400 950 
Wire Wire Line
	10400 1550 9700 1550
Text Notes 7550 700  0    50   ~ 10
Fans
Text Notes 7750 700  0    50   ~ 0
(PWM should be 25--30kHz and should accept 5V)
Text Notes 7600 850  0    50   ~ 0
Water block fans
Wire Notes Line
	10750 750  9750 750 
Wire Notes Line
	7550 750  9150 750 
Wire Notes Line
	10750 750  10750 1650
Wire Notes Line
	10750 1650 9750 1650
Wire Notes Line
	9750 1650 9750 750 
Wire Notes Line
	9150 750  9150 1650
Wire Notes Line
	9150 1650 7550 1650
Wire Notes Line
	7550 1650 7550 750 
$Comp
L power:GND #GND05
U 1 1 617617F7
P 3850 2700
F 0 "#GND05" H 3850 2450 50  0001 C CNN
F 1 "GND" H 3850 2550 50  0000 C CNN
F 2 "" H 3850 2700 50  0001 C CNN
F 3 "" H 3850 2700 50  0001 C CNN
	1    3850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2650 3850 2700
Wire Wire Line
	3850 2450 3850 2400
$Comp
L Device:C_Small C1
U 1 1 615B3D87
P 3850 2550
F 0 "C1" H 3850 2450 50  0000 L CNN
F 1 "100n" H 3850 2650 50  0000 L CNN
F 2 "" H 3850 2550 50  0001 C CNN
F 3 "~" H 3850 2550 50  0001 C CNN
	1    3850 2550
	1    0    0    1   
$EndComp
Text GLabel 4700 800  1    50   Input ~ 0
fpwm2
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R12
U 1 1 60297D6F
P 4700 2350
F 0 "R12" V 4700 2350 50  0000 C CNN
F 1 "150" V 4800 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4630 2350 50  0001 C CNN
F 3 "~" H 4700 2350 50  0001 C CNN
	1    4700 2350
	1    0    0    -1  
$EndComp
Text Label 4750 2750 0    50   ~ 0
A
Wire Wire Line
	4750 2750 5100 2750
Connection ~ 3050 3750
Wire Wire Line
	3050 3750 3750 3750
Wire Wire Line
	3750 3250 3750 3350
$Comp
L Device:Q_NPN_CBE Q3
U 1 1 5F95B743
P 3850 3550
F 0 "Q3" H 3850 3400 50  0000 R CNN
F 1 "BC547C" H 3850 3300 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4050 3475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC546-D.PDF" H 3850 3550 50  0001 L CNN
	1    3850 3550
	-1   0    0    -1  
$EndComp
$Comp
L cheesebox-rescue:Arduino_Nano_Every-symbol_arduino_nano_every-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue U4
U 1 1 5D7B0F33
P 5600 2450
F 0 "U4" H 5350 1500 50  0000 R CNN
F 1 "Arduino_Nano_Every" H 5350 1400 50  0000 R CNN
F 2 "Module:Arduino_Nano" H 5750 1500 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5600 1450 50  0001 C CNN
	1    5600 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4000 1600 4050 1600
Wire Wire Line
	4050 1600 4050 1650
Connection ~ 4050 1600
Wire Wire Line
	3650 2200 3650 2250
Wire Wire Line
	3650 1850 3650 1900
$Comp
L power:+5V #PWR12V05
U 1 1 611B0428
P 3650 1850
F 0 "#PWR12V05" H 3650 1700 50  0001 C CNN
F 1 "+5V" H 3750 1950 50  0000 C CNN
F 2 "" H 3650 1850 50  0001 C CNN
F 3 "" H 3650 1850 50  0001 C CNN
	1    3650 1850
	-1   0    0    -1  
$EndComp
Text GLabel 2550 2050 0    50   Input ~ 0
tdq
Text GLabel 2550 2150 0    50   Input ~ 0
tdq
Text GLabel 2550 2250 0    50   Input ~ 0
tdq
$Comp
L cheesebox-rescue:R-Device-cheesebox-rescue-cheesebox-rescue-cheesebox-rescue R10
U 1 1 5F7B7A04
P 4300 3550
F 0 "R10" V 4300 3550 50  0000 C CNN
F 1 "2k2" V 4400 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4230 3550 50  0001 C CNN
F 3 "~" H 4300 3550 50  0001 C CNN
	1    4300 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 3550 4150 3550
Wire Wire Line
	4550 3550 4450 3550
Wire Wire Line
	4250 1600 4250 2850
Wire Wire Line
	4250 2850 5100 2850
Wire Wire Line
	4050 1600 4250 1600
Wire Wire Line
	4150 2250 4150 2950
Wire Wire Line
	4150 2950 5100 2950
Wire Wire Line
	5100 3050 4050 3050
Wire Wire Line
	4050 3050 4050 2400
Wire Wire Line
	4050 2400 3850 2400
Wire Wire Line
	4400 800  4400 2200
Wire Wire Line
	4400 2500 4400 2650
Wire Wire Line
	4400 2650 4550 2650
Wire Wire Line
	4700 2200 4700 800 
Wire Wire Line
	4700 2500 4700 2550
Wire Wire Line
	4700 2550 5100 2550
Wire Wire Line
	4050 1850 4050 1900
Connection ~ 3850 2400
Wire Wire Line
	3700 1600 2550 1600
Wire Wire Line
	2550 2250 2650 2250
Wire Wire Line
	2550 2050 2650 2050
Wire Wire Line
	2650 2050 2650 2150
Connection ~ 2650 2250
Wire Wire Line
	2650 2250 3650 2250
Wire Wire Line
	2550 2150 2650 2150
Connection ~ 2650 2150
Wire Wire Line
	2650 2150 2650 2250
Wire Wire Line
	2550 2400 3850 2400
Wire Wire Line
	2550 3450 2700 3450
Wire Wire Line
	2550 3350 2650 3350
Wire Wire Line
	2650 3350 2650 2950
Wire Wire Line
	2650 2950 3050 2950
Wire Wire Line
	2550 3550 2650 3550
Wire Wire Line
	2650 3550 2650 3750
Wire Wire Line
	2650 3750 3050 3750
Wire Wire Line
	4550 3550 4550 3150
Wire Wire Line
	4550 3150 5100 3150
Connection ~ 3650 2250
Wire Wire Line
	3650 2250 4150 2250
Text Notes 5100 3050 2    50   ~ 0
ain
Wire Wire Line
	4850 800  4850 1350
Connection ~ 4850 1350
Wire Wire Line
	4850 1350 4850 2450
Text Notes 5100 2550 2    50   ~ 0
dout
Text Notes 5100 2850 2    50   ~ 0
din
Text Notes 5100 2650 2    50   ~ 0
dout
Text Notes 5100 2750 2    50   ~ 0
dout
Text Notes 5100 2950 2    50   ~ 0
din
Text Notes 5100 2450 2    50   ~ 0
ain
Text Notes 5100 3150 2    50   ~ 0
dout
$Comp
L Device:R R7
U 1 1 611AEA07
P 3650 2050
F 0 "R7" H 3700 2100 50  0000 L CNN
F 1 "4k7" H 3700 2000 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3580 2050 50  0001 C CNN
F 3 "~" H 3650 2050 50  0001 C CNN
	1    3650 2050
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
