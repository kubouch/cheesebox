# Cheesebox control logic

## Build & Upload

The ATMega4809 chip is not supported by the native toolchain (avr-gcc, avrdude).
Therefore, Arduino is required to be installed (see Makefile for assumed paths)

Before uploading, the board's serial port connection has to be opened and closed with baud rate 1200.
See reset_serial.sh and make program rule for details.
This assumes Putty to be installed.
