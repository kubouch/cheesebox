#include "display.h"

#include <avr/io.h>

/* Round number to the order of tens (e.g. 2345 becomes 2350) */
static uint16_t round_to_tens(uint16_t num)
{
        uint16_t floored = (num / 10) * 10;

        if (num - floored >= 5)
        {
                floored += 10;
        }

        return floored;
}

void display_symbol(uint8_t symbol, bool dp)
{
        switch (symbol) {
        case 0:
                DISP_0();
                break;
        case 1:
                DISP_1();
                break;
        case 2:
                DISP_2();
                break;
        case 3:
                DISP_3();
                break;
        case 4:
                DISP_4();
                break;
        case 5:
                DISP_5();
                break;
        case 6:
                DISP_6();
                break;
        case 7:
                DISP_7();
                break;
        case 8:
                DISP_8();
                break;
        case 9:
                DISP_9();
                break;
        case 10:
                DISP_A();
                break;
        case 11:
                DISP_B();
                break;
        case 12:
                DISP_C();
                break;
        case 13:
                DISP_D();
                break;
        case 14:
                DISP_E();
                break;
        case 15:
                DISP_F();
                break;
        case MINUS:
                DISP_MINUS();
                break;
        case NONE:
                DISP_NONE();
                break;
        case ERR:
        default:
                DISP_ERR();
                break;
        }

        if (dp)
        {
                SET_DP();
        }
}

void display_on(void)
{
        // enable PWM output on the "DIGIT_ON" pin
        TCA0.SINGLE.CTRLB |= TCA_SINGLE_CMP0EN_bm;
}

void display_off(void)
{
        // disable PWM output on the "DIGIT_ON" pin and set it to 0
        TCA0.SINGLE.CTRLB &= ~TCA_SINGLE_CMP0EN_bm;
        CLR_PIN(DIGIT_ON_PORT, DIGIT_ON_PIN);
}

void activate_pos(uint8_t pos)
{
        switch (pos) {
        case 0:
        default:
                CLR_PIN(DIGIT_SEL0_PORT, DIGIT_SEL0_PIN);
                CLR_PIN(DIGIT_SEL1_PORT, DIGIT_SEL1_PIN);
                break;
        case 1:
                SET_PIN(DIGIT_SEL0_PORT, DIGIT_SEL0_PIN);
                CLR_PIN(DIGIT_SEL1_PORT, DIGIT_SEL1_PIN);
                break;
        case 2:
                CLR_PIN(DIGIT_SEL0_PORT, DIGIT_SEL0_PIN);
                SET_PIN(DIGIT_SEL1_PORT, DIGIT_SEL1_PIN);
                break;
        case 3:
                SET_PIN(DIGIT_SEL0_PORT, DIGIT_SEL0_PIN);
                SET_PIN(DIGIT_SEL1_PORT, DIGIT_SEL1_PIN);
                break;
        }
}

void display_init(void)
{
        // Setup segments of 7-seg display
        PIN_DIR_OUT(SEG_A_PORT, SEG_A_PIN);
        PIN_DIR_OUT(SEG_B_PORT, SEG_B_PIN);
        PIN_DIR_OUT(SEG_C_PORT, SEG_C_PIN);
        PIN_DIR_OUT(SEG_D_PORT, SEG_D_PIN);
        PIN_DIR_OUT(SEG_E_PORT, SEG_E_PIN);
        PIN_DIR_OUT(SEG_F_PORT, SEG_F_PIN);
        PIN_DIR_OUT(SEG_G_PORT, SEG_G_PIN);
        PIN_DIR_OUT(SEG_DP_PORT, SEG_DP_PIN);

        // Setup digit select pins of 7-seg display / multiplexer
        PIN_DIR_OUT(DIGIT_ON_PORT, DIGIT_ON_PIN);
        PIN_DIR_OUT(DIGIT_SEL0_PORT, DIGIT_SEL0_PIN);
        PIN_DIR_OUT(DIGIT_SEL1_PORT, DIGIT_SEL1_PIN);

        display_symbol(NONE, false);
        activate_pos(0);
        display_on();
}

void display_symbol_pos(uint8_t symbol, bool dp, uint8_t pos)
{
        display_off();
        display_symbol(symbol, dp);
        activate_pos(pos);
        display_on();
}

void display_fakefp(int16_t num)
{
        static uint8_t symbol_pos = 0;
        uint16_t num_disp;
        uint8_t symbol, digit;
        bool dp = false;

        if (num < 0) {
                num_disp = round_to_tens(-num);
        } else {
                num_disp = round_to_tens(num);
        }

        if (num < -9994)
        {
                symbol = ERR;
                goto start_display;
        }

        switch (symbol_pos) {
        case 0:
        default:
                if (num < 0)
                {
                        // "-" when negative number
                        symbol = MINUS;
                }
                else
                {
                        digit = num_disp / 10000 % 10;
                        if (digit == 0) {
                                // skip zero
                                symbol = NONE;
                        } else {
                                symbol = digit;
                        }
                }
                break;
        case 1:
                digit = num_disp / 1000 % 10;
                if (digit == 0 && num_disp < 1000) {
                        // skip zero if in the beginning of the number
                        symbol = NONE;
                } else {
                        symbol = digit;
                }
                break;
        case 2:
                symbol = num_disp / 100 % 10;
                dp = true;
                break;
        case 3:
                symbol = num_disp / 10 % 10;
                break;
        }

start_display:
        display_symbol_pos(symbol, dp, symbol_pos);

        if (++symbol_pos > 3)
        {
                symbol_pos = 0;
        }
}

void display_byte_hex(uint8_t byte, uint8_t pos)
{
        static uint8_t symbol_pos = 0;
        uint8_t symbol;
        bool dp = false;

        // max. value of `pos` is 99 to fit two digits
        if (pos > 99 && symbol_pos <= 1)
        {
                symbol = MINUS;
                goto start_display;
        }

        switch (symbol_pos) {
        case 0:
        default:
                if (pos < 10) {
                        // numbers <10 will be displayed as first digit
                        symbol = pos % 10;
                }
                else
                {
                        symbol = pos / 10 % 10;
                }
                break;
        case 1:
                if (pos < 10)
                {
                        symbol = NONE;
                }
                else
                {
                        symbol = pos % 10;
                }
                dp = true;
                break;
        case 2:
                symbol = (byte & 0xf0) >> 4;
                break;
        case 3:
                symbol = (byte & 0x0f);
                break;
        }

start_display:
        display_symbol_pos(symbol, dp, symbol_pos);

        if (++symbol_pos > 3)
        {
                symbol_pos = 0;
        }
}
