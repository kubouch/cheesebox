/* Macros and functions for displaying segments of one 7-segment display */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdbool.h>

#include "gpio.h"

// segment pin definitions
#define SEG_A_PORT   PORTA    // a, L1
#define SEG_A_PIN    PIN0_bm
#define SEG_B_PORT   PORTF    // b, L2
#define SEG_B_PIN    PIN5_bm
#define SEG_C_PORT   PORTC    // c, L3
#define SEG_C_PIN    PIN6_bm
#define SEG_D_PORT   PORTB    // d
#define SEG_D_PIN    PIN2_bm
#define SEG_E_PORT   PORTF    // e
#define SEG_E_PIN    PIN4_bm
#define SEG_F_PORT   PORTA    // f
#define SEG_F_PIN    PIN1_bm
#define SEG_G_PORT   PORTE    // g
#define SEG_G_PIN    PIN3_bm
#define SEG_DP_PORT  PORTB    // dp
#define SEG_DP_PIN   PIN0_bm

// digit pin definitions           mux pin:
#define DIGIT_ON_PORT    PORTD     // A
#define DIGIT_ON_PIN     PIN0_bm
#define DIGIT_SEL0_PORT  PORTB     // S0
#define DIGIT_SEL0_PIN   PIN1_bm
#define DIGIT_SEL1_PORT  PORTE     // S1
#define DIGIT_SEL1_PIN   PIN0_bm

// turn segment on
#define SET_A()   SET_PIN(SEG_A_PORT, SEG_A_PIN)
#define SET_B()   SET_PIN(SEG_B_PORT, SEG_B_PIN)
#define SET_C()   SET_PIN(SEG_C_PORT, SEG_C_PIN)
#define SET_D()   SET_PIN(SEG_D_PORT, SEG_D_PIN)
#define SET_E()   SET_PIN(SEG_E_PORT, SEG_E_PIN)
#define SET_F()   SET_PIN(SEG_F_PORT, SEG_F_PIN)
#define SET_G()   SET_PIN(SEG_G_PORT, SEG_G_PIN)
#define SET_DP()  SET_PIN(SEG_DP_PORT, SEG_DP_PIN)

// turn segment off
#define CLR_A()   CLR_PIN(SEG_A_PORT, SEG_A_PIN)
#define CLR_B()   CLR_PIN(SEG_B_PORT, SEG_B_PIN)
#define CLR_C()   CLR_PIN(SEG_C_PORT, SEG_C_PIN)
#define CLR_D()   CLR_PIN(SEG_D_PORT, SEG_D_PIN)
#define CLR_E()   CLR_PIN(SEG_E_PORT, SEG_E_PIN)
#define CLR_F()   CLR_PIN(SEG_F_PORT, SEG_F_PIN)
#define CLR_G()   CLR_PIN(SEG_G_PORT, SEG_G_PIN)
#define CLR_DP()  CLR_PIN(SEG_DP_PORT, SEG_DP_PIN)

// display digit or symbol
#define DISP_0()  SET_A(); SET_B(); SET_C(); SET_D(); SET_E(); SET_F(); CLR_G(); CLR_DP()
#define DISP_1()  CLR_A(); SET_B(); SET_C(); CLR_D(); CLR_E(); CLR_F(); CLR_G(); CLR_DP()
#define DISP_2()  SET_A(); SET_B(); CLR_C(); SET_D(); SET_E(); CLR_F(); SET_G(); CLR_DP()
#define DISP_3()  SET_A(); SET_B(); SET_C(); SET_D(); CLR_E(); CLR_F(); SET_G(); CLR_DP()
#define DISP_4()  CLR_A(); SET_B(); SET_C(); CLR_D(); CLR_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_5()  SET_A(); CLR_B(); SET_C(); SET_D(); CLR_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_6()  SET_A(); CLR_B(); SET_C(); SET_D(); SET_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_7()  SET_A(); SET_B(); SET_C(); CLR_D(); CLR_E(); CLR_F(); CLR_G(); CLR_DP()
#define DISP_8()  SET_A(); SET_B(); SET_C(); SET_D(); SET_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_9()  SET_A(); SET_B(); SET_C(); SET_D(); CLR_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_A()  SET_A(); SET_B(); SET_C(); CLR_D(); SET_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_B()  CLR_A(); CLR_B(); SET_C(); SET_D(); SET_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_C()  SET_A(); CLR_B(); CLR_C(); SET_D(); SET_E(); SET_F(); CLR_G(); CLR_DP()
#define DISP_D()  CLR_A(); SET_B(); SET_C(); SET_D(); SET_E(); CLR_F(); SET_G(); CLR_DP()
#define DISP_E()  SET_A(); CLR_B(); CLR_C(); SET_D(); SET_E(); SET_F(); SET_G(); CLR_DP()
#define DISP_F()  SET_A(); CLR_B(); CLR_C(); CLR_D(); SET_E(); SET_F(); SET_G(); CLR_DP()

#define DISP_MINUS()  CLR_A(); CLR_B(); CLR_C(); CLR_D(); CLR_E(); CLR_F(); SET_G(); CLR_DP()
#define DISP_NONE()   CLR_A(); CLR_B(); CLR_C(); CLR_D(); CLR_E(); CLR_F(); CLR_G(); CLR_DP()
#define DISP_ERR()    SET_A(); CLR_B(); CLR_C(); SET_D(); CLR_E(); CLR_F(); SET_G(); CLR_DP()

// special non-digit symbols
#define MINUS  16
#define NONE   17
#define ERR    255

/* Display a symbol. For non-digit symbols, use macros defining them above:
 *   - single digit 0..9
 *   - minus '-' ( display_symbol(MINUS) )
 *   - nothing  ( display_symbol(NONE) )
 *
 * The displayed symbol overwrites all segments.
 * To display a dot point, set `dp` to true.
 */
void display_symbol(uint8_t symbol, bool dp);

/* Turn display on */
void display_on(void);

/* Turn display off */
void display_off(void);

/* Activate a digit on a position `pos` */
void activate_pos(uint8_t pos);

/* Display a symbol at position `pos` */
void display_symbol_pos(uint8_t symbol, bool dp, uint8_t pos);

/* Init display pins etc. */
void display_init(void);

/* Update display to show one digit of a number and switch to the next digit.
 *
 * Should be called fairly often (e.g. 2 ms).
 *
 * The number is supposed to be in a custom "fake floating point" format:
 * The last two digits are decimal, the rest is signed integer. For example,
 * "-1234" corresponds to "-12.34" and displays rounded as "-12.3".
 *
 * Displays values -9994 to 32767 as "-99.9" to "327.7".
 *   < -9994 displays error
 *   > 32767 is integer overflow
 */
void display_fakefp(int16_t num);

/* Update display to show one symbol of a byte (in hex) and switch to the next.
 *
 * The byte hex occupies the two last digits. The first two digits display the
 * `pos` value in decimal which can denote, e.g., the position of the byte.
 */
void display_byte_hex(uint8_t byte, uint8_t pos);

#endif // DISPLAY_H
