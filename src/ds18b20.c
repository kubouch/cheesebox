/** Library for reading temperature from DS18B20 1-wire sensor.
 *
 * Adapted from "Using DS18B20 digital temperature sensor on AVR
 * microcontrollers" by Gerard Marull Paretas:
 * https://teslabs.com/openplayer/docs/docs/other/ds18b20_pre1.pdf
 *
 * Also used "Build a Dual-Temperature Maxim DS18B20 Thermometer" by Bruce E.
 * Hall (http://w8bh.net) as a reference for multi-sensor support.
 */

#include <stddef.h>

#include <util/atomic.h>
#include <util/delay.h>

#include "ds18b20.h"

static uint8_t therm_reset(void)
{
        uint8_t check;

        // Pull line low and wait for 480uS
        THERM_LOW();
        THERM_OUTPUT_MODE();
        _delay_us(480);

        // Release line and wait for 60uS
        THERM_INPUT_MODE();
        _delay_us(60);

        // Store line value and wait until the completion of 480us period
        check = THERM_READ_DQ();
        _delay_us(420);

        // Return the value read from the presence pulse (0=OK, 1=WRONG)
        return check;
}

static void therm_write_bit(uint8_t bit)
{
        // Pull line low for 1us
        THERM_LOW();
        THERM_OUTPUT_MODE();
        _delay_us(1);

        // If we want to write 1, release the line (if not will keep low)
        if (bit) THERM_INPUT_MODE();

        // Wait for 60uS and release the line
        _delay_us(60);
        THERM_INPUT_MODE();
}

static uint8_t therm_read_bit(void)
{
        uint8_t bit = 0;

        // Pull line low for 1us
        THERM_LOW();
        THERM_OUTPUT_MODE();
        _delay_us(1);

        // Release line and wait for 14us
        THERM_INPUT_MODE();
        _delay_us(14);

        // Read line value
        if (THERM_READ_DQ()) bit = 1;

        // Wait for 45us to end and return read value
        _delay_us(45);
        return bit;
}

static void therm_write_byte(uint8_t byte)
{
        uint8_t i = 8;

        while(i--) {
                // Write actual bit and shift one position right to make next bit ready
                therm_write_bit(byte & 1);
                byte >>= 1;
        }
}

static uint8_t therm_read_byte(void)
{
        uint8_t i = 8;
        uint8_t n = 0;

        while(i--) {
                // Shift onse position right and store read value
                n >>= 1;
                n |= (therm_read_bit() << 7);
        }

        return n;
}

int16_t read_tmpr(const uint8_t *id)
{
        uint8_t buf[2];

        /* Reset, skip or match ROM, temp convertion */
        therm_reset();
        if (id == NULL)
        {
                therm_write_byte(THERM_CMD_SKIPROM);
        }
        else
        {
                therm_write_byte(THERM_CMD_MATCHROM);
                for (uint8_t i = 0; i < 8; ++i)
                {
                        therm_write_byte(id[i]);
                }
        }
        therm_write_byte(THERM_CMD_CONVERTTEMP);

        /* Wait until conversion completion */
        while (!therm_read_bit());

        /* Reset, skip or match ROM, send cmd to read Scratchpad */
        therm_reset();
        if (id == NULL)
        {
                therm_write_byte(THERM_CMD_SKIPROM);
        }
        else
        {
                therm_write_byte(THERM_CMD_MATCHROM);
                for (uint8_t i = 0; i < 8; ++i)
                {
                        therm_write_byte(id[i]);
                }
        }
        therm_write_byte(THERM_CMD_RSCRATCHPAD);

        /* Read Scratchpad (only the first 2 bytes) */
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
        {
                buf[0] = therm_read_byte();
                buf[1] = therm_read_byte();
        }
        therm_reset();

        /* integer part (incl. sign bit) */
        int16_t integer = (int8_t)( (buf[1] << 4) | (buf[0] >> 4) );

        /* decimal part */
        int16_t decimal = THERM_DECIMAL_STEPS_12BIT * (buf[0] & 0x0f);

        return (integer * 100) + (decimal / 100);
}

uint8_t read_id(uint8_t *id)
{
        therm_reset();
        therm_write_byte(THERM_CMD_READROM);

        for (uint8_t i = 0; i < 8; ++i)
        {
                id[i] = therm_read_byte();
        }

        therm_reset();

        // TODO: implement CRC check
        return 0;
}
