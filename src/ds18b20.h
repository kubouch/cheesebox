/** Library for reading temperature from DS18B20 1-wire sensor.
 *
 * Adapted from "Using DS18B20 digital temperature sensor on AVR
 * microcontrollers" by Gerard Marull Paretas:
 * https://teslabs.com/openplayer/docs/docs/other/ds18b20_pre1.pdf
 *
 * Also used "Build a Dual-Temperature Maxim DS18B20 Thermometer" by Bruce E.
 * Hall (http://w8bh.net) as a reference for multi-sensor support.
 */

#ifndef _DS18B20_H
#define _DS18B20_H

#include "gpio.h"

/* Thermometer Connections (ATMega4809-specific) */
#define THERM_PORT  PORTA
#define THERM_PIN   PIN3_bm

/* Utils (ATMega4809-specific) */
#define THERM_INPUT_MODE()   PIN_DIR_IN(THERM_PORT, THERM_PIN)
#define THERM_OUTPUT_MODE()  PIN_DIR_OUT(THERM_PORT, THERM_PIN)
#define THERM_LOW()          CLR_PIN(THERM_PORT, THERM_PIN)
#define THERM_HIGH()         SET_PIN(THERM_PORT, THERM_PIN)
#define THERM_READ_DQ()      READ_PIN(THERM_PORT, THERM_PIN)

/* Function and ROM commands */
#define THERM_CMD_CONVERTTEMP    0x44
#define THERM_CMD_RSCRATCHPAD    0xbe
#define THERM_CMD_WSCRATCHPAD    0x4e
#define THERM_CMD_CPYSCRATCHPAD  0x48
#define THERM_CMD_RECEEPROM      0xb8
#define THERM_CMD_RPWRSUPPLY     0xb4
#define THERM_CMD_SEARCHROM      0xf0
#define THERM_CMD_READROM        0x33
#define THERM_CMD_MATCHROM       0x55
#define THERM_CMD_SKIPROM        0xcc
#define THERM_CMD_ALARMSEARCH    0xec

/* Thermometer precision (stored in the config byte) */
#define THERM_DECIMAL_STEPS_12BIT  625  // .0625 (default)

/* Read temperature from sensor and return it as a signed integer with the last
 * two digits assumed to be decimals (i.e. -1234 is in fact -12.34).
 *
 * If `id` is not a NULL pointer, use it to match it to the exact device on the
 * 1-Wire bus.
 */
int16_t read_tmpr(const uint8_t *id);

/* Read the 64-bit ID of a sensor from its ROM
 *
 * !! Only one device can be connected at a time !!
 *
 * Returns 0 - OK, 1 - CRC check error
 *
 * TODO: implement CRC check
 */
uint8_t read_id(uint8_t *id);

#endif /* _DS18B20_H */
