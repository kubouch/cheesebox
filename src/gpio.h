#ifndef GPIO_H
#define GPIO_H

#include <avr/io.h>

// General pin operation
#define PIN_DIR_OUT(port, pin_bm)  port.DIR |= pin_bm
#define PIN_DIR_IN(port, pin_bm)   port.DIR &= ~pin_bm

#define SET_PIN(port, pin_bm)      port.OUT |= pin_bm
#define CLR_PIN(port, pin_bm)      port.OUT &= ~pin_bm

#define READ_PIN(port, pin_bm)     port.IN & pin_bm

// Pin definitions
#define LED_PORT  PORTE
#define LED_PIN   PIN2_bm

#define PWM_7SEG_PORT  PORTD
#define PWM_7SEG_PIN   PIN0
#define PWM_FAN1_PORT  PORTD
#define PWM_FAN1_PIN   PIN1
#define PWM_FAN2_PORT  PORTD
#define PWM_FAN2_PIN   PIN2

#endif // GPIO_H
