#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>

#include <stdbool.h>

#include "gpio.h"
#include "display.h"
#include "ds18b20.h"

#define PERIOD_MS_THERM  1000  // temperature measurement period

/* Global counter to measure delay (updated by e.g. an interrupt) */
volatile uint16_t tick_cnt;

/* Addresses of temperature sensors */
const uint8_t therm_id[3][8] = {
        { 0x28, 0x15, 0xc7, 0x23, 0x30, 0x14, 0x01, 0x72 },  // 0
        { 0x28, 0xbb, 0xcf, 0xc0, 0x0b, 0x00, 0x00, 0x54 },  // 1
        { 0x28, 0x16, 0xad, 0xc1, 0x0b, 0x00, 0x00, 0x5f }   // 2
};

/* Temperatures of the above sensors */
int16_t tmpr[3] = { 0, 0, 0 };

/* Which temperature to display */
uint8_t tmpr_display_num = 0;

/* Check whether a `tick_cnt` surpassed `delay_cnt` by at least `delay` number
 * of ticks. Optionally reset after the check.
 *
 * Original idea by Dean Camera (www.lufa-lib.org, 2012), adapted by AP 2014
 */
bool has_delay_elapsed(uint16_t delay, uint16_t *delay_cnt, bool reset)
{
        uint16_t current_tick;
        uint16_t current_cnt;

        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
        {
                current_tick = tick_cnt;
        }

        current_cnt = *delay_cnt;

        if (current_tick >= current_cnt)
        {
                if (current_tick - current_cnt >= delay)
                {
                        if (reset) *delay_cnt = current_tick;
                        return true;
                }
        }
        else
        {
                if ( (0xFFFF - current_cnt) + current_tick >= delay )
                {
                        if (reset) *delay_cnt = current_tick;
                        return true;
                }
        }

        return false;
}

/* Measure temperature of all sensors */
void measure_tmpr(uint16_t period_ms)
{
        static uint16_t delay_cnt = 0;
        if (!has_delay_elapsed(period_ms, &delay_cnt, true)) return;

        SET_PIN(LED_PORT, LED_PIN);
        tmpr[0] = read_tmpr(therm_id[0]);
        tmpr[1] = read_tmpr(therm_id[1]);
        tmpr[2] = read_tmpr(therm_id[2]);
        CLR_PIN(LED_PORT, LED_PIN);
}

/* Find the ID of a connected DS18B20 sensor and print it on 7seg display.
 *
 * This enters an infinite loop!
 * Only one sensor can be connected at a time!
 */
void display_therm_id()
{
        uint8_t id[8];
        read_id(id);

        // Print loop
        uint16_t delay_ms = 2000;
        uint8_t byte_cnt = 0;
        uint16_t ms_cnt = 0;
        while (1) {
                display_byte_hex(id[byte_cnt], byte_cnt);
                _delay_ms(1);

                if (++ms_cnt >= delay_ms)
                {
                        ms_cnt = 0;
                        byte_cnt = (byte_cnt + 1) % 8;
                }
        }
}

int main(void)
{
        tick_cnt = 0;

        // Enable clock prescaler /2 (=> 8MHz clock)
        _PROTECTED_WRITE(
                CLKCTRL.MCLKCTRLB,
                CLKCTRL_PEN_bm | CLKCTRL_PDIV_2X_gc
        );

        // LED as output
        PIN_DIR_OUT(LED_PORT, LED_PIN);

        // Init 7-segment display pins
        display_init();

        // Setup PWM pins as output
        PIN_DIR_OUT(PWM_7SEG_PORT, PWM_7SEG_PIN);  // TCA0 WO0
        /* PIN_DIR_OUT(PWM_FAN1_PORT, PWM_FAN1_PIN);  // TCA0 WO1 */
        /* PIN_DIR_OUT(PWM_FAN2_PORT, PWM_FAN2_PIN);  // TCA0 WO2 */

        // Timer A (TCA0) setup for PWM
        PORTMUX.TCAROUTEA |= PORTMUX_TCA0_PORTD_gc;  // TCA pins on PD[5:0]
        TCA0.SINGLE.CTRLA |=
                TCA_SINGLE_CLKSEL_DIV1_gc            // clock prescaler
                | TCA_SINGLE_ENABLE_bm;              // enable TCA0
        TCA0.SINGLE.CTRLB |=
                TCA_SINGLE_WGMODE_SINGLESLOPE_gc     // single-slope PWM
                | TCA_SINGLE_CMP0EN_bm;               // 7seg brightness PWM
                // | TCA_SINGLE_CMP1EN_bm;              // fan speed PWM
                // | TCA_SINGLE_CMP2EN_bm;              // fan speed PWM
        TCA0.SINGLE.PER = 320;  // number of clock cycles until 40us (25 kHz)
        TCA0.SINGLE.CMP0 = 80;  // duty cycle

        // Timer B (TCB0) setup to interrupt every 1 ms
        TCB0.CCMP = 8000;             // number of clock cycles until 1 ms
        TCB0.INTCTRL |= TCB_CAPT_bm;  // enable interrupt
        TCB0.CTRLA |= TCB_ENABLE_bm;  // enable timer

        // Print out thermal sensor address (infinite loop)
        /* display_therm_id(); */

        // Make the first measurement before enabling display
        measure_tmpr(0);

        // Global enable interrupts
        sei();

        while (1) {
                measure_tmpr(PERIOD_MS_THERM);
        }
}

ISR(TCB0_INT_vect)
{
        ++tick_cnt;
        display_fakefp(tmpr[tmpr_display_num]);
        TCB0.INTFLAGS |= TCB_CAPT_bm;  // clear interrupt flags
}
