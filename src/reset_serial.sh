#!/usr/bin/env bash
#
# Reset serial port provided as command line argument

timeout 0.5 putty -serial "$1" -sercfg 1200
exit 0
